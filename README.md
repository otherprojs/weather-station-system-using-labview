#A Weather Station system using LabVIEW:

1. Acquires a temperature every half a second
2. Analyzes the temperature to determine if it is too high or too low
3. Alerts the user if there is a danger of heat stroke or freezing
4. Displays the data to the user
5. The user has the ability to stop the program at any moment by pressing a button

##Inputs:

• Current Temperature (T) (From Sensor (LM35 Temperature Sensor))

• High Temperature Limit (Input from user on the Front Panel)

• Low Temperature Limit (Input from user on the Front Panel)

• Stop

##Outputs:

• Warning Levels: Heatstroke Warning, Freeze Warning, No Warning (String indicator on the Front Panel)

• Current Temperature Display (Graph indicator on the Front Panel) 
